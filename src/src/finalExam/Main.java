package finalExam;

public class Main {

    public static void main(String[] args) {
        User Alice = new User("Alice", "123456", "alice@forum.bg", "14.02.2019", "19:00", 3);
        User Bob = new User("Bob", "123456", "bob@forum.bg", "14.02.2019", "19:05", 1);
        User Charlie = new User("Charlie", "123456", "charlie@forum.bg", "14.02.2019", "19:10", 1);

        Bob.createTopic("First topic", "First msg.");
        Charlie.createPost("First post","14.02.2019", "First topic");
    }
}

