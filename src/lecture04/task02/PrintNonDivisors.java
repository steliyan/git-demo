package lecture04.task02;

import java.util.Scanner;

public class PrintNonDivisors {
    public static void main(String[] args) {
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        for (int num = 1; num <= input; num++) {
            if (num % 3 != 0 && num % 7 != 0) {
                System.out.println(num);
            }
        }

    }
}