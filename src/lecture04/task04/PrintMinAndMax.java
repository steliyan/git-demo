package lecture04.task04;

import java.util.Scanner;

public class PrintMinAndMax {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int min = 0;
        int max = 0;

        for (int i = 0; i < n; i++) {
            int tmp = sc.nextInt();
            if (i == 0) {
                min = tmp;
                max = tmp;
            }
            if (min > tmp) {
                min = tmp;
            }
            if (max < tmp) {
                max = tmp;
            }
        }
            System.out.println(min);
            System.out.println(max);

    }
}
