package lecture04.task05;

import java.util.Scanner;

public class PrintSumOfN {
    public static void main(String[] args) {
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num = 0;
        int input = sc.nextInt();
        int sum = 0;
        do {
            sum += sc.nextInt();
            num++;
        }while(input>num);
        System.out.println(sum);
    }
}