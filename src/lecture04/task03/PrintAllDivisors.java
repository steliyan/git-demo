package lecture04.task03;

import java.util.Scanner;

public class PrintAllDivisors {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number :  ");
        int y = s.nextInt(), i;
        System.out.print("Divisors of " + y + " = 1 , ");

        for (i = 2; i < y; i++) {
            int z = y % i;
            if (z != 0) continue;
            System.out.print(i + " , ");

        }
        System.out.println(y);
    }
}
