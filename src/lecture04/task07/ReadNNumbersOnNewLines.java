package lecture04.task07;

import java.util.Scanner;

public class ReadNNumbersOnNewLines {
    public static void main(String[] args) {
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        String oneLineNumbers = "";
        for (num = 0; num < input; num++) {
            if (num > 0) {
                sc.nextLine();
            }
            oneLineNumbers += sc.nextInt() + " ";
        }
        System.out.println(oneLineNumbers);
    }
}
