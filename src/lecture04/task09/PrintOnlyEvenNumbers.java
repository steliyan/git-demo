package lecture04.task09;

import java.util.Scanner;

public class PrintOnlyEvenNumbers {
    public static void main(String[] args) {
        System.out.print("Input number:");
        Scanner sc = new Scanner(System.in);
        int num;
        int input = sc.nextInt();
        String oneLineNumbers = "";
        for(num=0;num<input;num++){
            if(num%2!=0) {
                oneLineNumbers += sc.nextInt() + " ";
            }
        }
        System.out.println(oneLineNumbers);

    }
}
