package lecture04.task00;

import java.util.Calendar;
import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String firstName;
        String lastName;
        String profession;
        String information = "";
        double weight;
        int input;
        double height;
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        short birthDate;
        System.out.println("How many ppl u will register ?:");
        do {
            input = sc.nextInt();
        } while (input > 5);

        for (int count = 1; input >= count; count++) {
            System.out.println("First name:");
            do {
                firstName = sc.nextLine();
            } while (firstName.equals("") || firstName.equals(" "));
            System.out.println("Last name:");
            do {
                lastName = sc.nextLine();
            } while (lastName.equals("") || lastName.equals(" "));
            System.out.println("Birth Year:");
            do {
                birthDate = sc.nextShort();
            } while ((birthDate < 1850) || (birthDate > currentYear));
            System.out.println("Your weight(kg):");
            do {
                weight = sc.nextDouble();
            } while (weight <= 15);
            System.out.println("Your height (cm):");
            do {
                height = sc.nextDouble();
            } while (height <= 15);
            System.out.println("Your profession:");
            sc.nextLine(); // Fix skip String profession's sc.nextLine();
            do {
                profession = sc.nextLine();
            } while (profession.equals("") || profession.equals(" "));
            information += firstName + " " + lastName + " is " + (currentYear - birthDate) + " years old. His weight is " + weight + " kg and he is " + height + " cm tall. He is a " + profession + ". ";
            System.out.println(firstName + " " + lastName + " is " + (currentYear - birthDate) + " years old. His weight is " + weight + " kg and he is " + height + " cm tall. He is a " + profession + ".");
            if (currentYear - birthDate <= 18) {
                System.out.println(firstName + " " + lastName + " is under-aged.");
                information += firstName + " " + lastName + " is under-aged. ";
            }
        }
        System.out.println(information);
    }

}