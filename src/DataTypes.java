public class DataTypes {
    public static void main(String[] args) {
        int i = 30014;
        System.out.println(i);
        short s = 131;
        System.out.println(s);
        long l = 9649613133132L;
        System.out.println(l);
        byte b = 53;
        System.out.println(b);
        double d = 2564.25D;
        System.out.println(d);
        float f = 3410.0215F;
        System.out.println(f);
        char c = 'g';
        System.out.println(c);
        boolean bo = false;
        System.out.println(bo);
    }
}
