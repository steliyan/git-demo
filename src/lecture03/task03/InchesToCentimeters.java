package lecture03.task03;

import java.util.Scanner;

public class InchesToCentimeters {
    public static void main(String[] args){

        Scanner s = new Scanner(System.in);
        System.out.print("Enter inches: ");

        double i = s.nextDouble();
        double c = i * 2.54;

        System.out.println("In centimeters: " + c);

    }
}
