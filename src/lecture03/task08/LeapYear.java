package lecture03.task08;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.print("Input year");
        int year = s.nextInt();
        boolean isLeapYear = (year % 100 == 0) ? (year % 400 == 0) : (year % 4 == 0);

        System.out.println(isLeapYear);
    }
}
