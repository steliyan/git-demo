package lecture03.task07;

import java.util.Scanner;

public class SecondsInBiggerIntervals {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        int minutesSeconds = 60;
        int hoursSeconds = minutesSeconds * 60;
        int daysSeconds = hoursSeconds * 24;
        int seconds, minutes, hours, days;

        System.out.print("Input seconds: ");
        int inputSeconds = s.nextInt();

        if (inputSeconds < 0) {
            System.out.println("Error");
        } else {
            days = inputSeconds / daysSeconds;
            hours = (inputSeconds % daysSeconds) / hoursSeconds;
            minutes = ((inputSeconds % daysSeconds) % hoursSeconds) / minutesSeconds;
            seconds = ((inputSeconds % daysSeconds) % hoursSeconds) % minutesSeconds;
            System.out.println("Days: " + days + " Hours: " + hours + " Minutes: " + minutes + " Seconds: " + seconds);
        }
    }
}
