package lecture03.task06.a;

import java.util.Scanner;

public class SwapVariables {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.print("Enter fist number: ");
        int a = s.nextInt();

        System.out.print("Enter second number: ");
        int b = s.nextInt();

        int temp;
        temp = a;
        a = b;
        b = temp;

        System.out.println(a + " " + b);
    }
}
