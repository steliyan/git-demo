package lecture03.task04;

import java.util.Scanner;

public class OddOrEven {
    public static void main(String[] args) {

        double number;

        Scanner s = new Scanner(System.in);

        System.out.print("Enter a number: ");
        number = s.nextDouble();

        if (number % 2 == 0) {
            System.out.println("Result: even");

        } else {
            System.out.println("Result: odd");
        }
    }
}

