package lecture03.task00;

public class PersonCharacteristics {
    public static void main(String[] args) {
        String firstName = "Steliyan";
        String lastName = "Stanchev";
        int brithDate = 1997;
        double weight = 110;
        double height = 195.0;
        String proffesion = "Student";

        System.out.println(firstName + " " + lastName + " is " + (2018 - brithDate) + " years old. His weight is " + weight + " kg and he is " + height + " cm tall. He is a " + proffesion + ".");
    }
}
