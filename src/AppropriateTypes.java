public class AppropriateTypes {
    public static void main(String[] args) {
        byte a = 43;
        System.out.println(a);
        char g = 'M';
        System.out.println(g);
        char name = 'G';
        System.out.println(name);
        double w = 201.0D;
        System.out.println(w);
        byte e = 5;
        System.out.println(e);
        byte s = 40;
        System.out.println(s);
    }
}
