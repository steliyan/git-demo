package bg.swift.socialsystem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Collections;
import java.util.List;

@Entity
public class Education {

    @Id
    @GeneratedValue
    private Integer id_edu;
    private Character educationType;
    private String institutionName;
    private String enrollmentDate;
    private String graduationDate;
    private String hasGraduated;
    private Double finalGrades;

    Education() {

    }

    Education(char edType, String insName, String enrollDate, String gradDate, String isGraduated, Double finGrades) {
        educationType = edType;
        institutionName = insName;
        enrollmentDate = enrollDate;
        graduationDate = gradDate;
        hasGraduated = isGraduated;
        finalGrades = finGrades;
    }
    @Override
    public String toString() {
        return "Education{" +
                "id=" + id_edu +
                ", edType='" + educationType + '\'' +
                ", iname='" + institutionName + '\'' +
                ", enrollDate='" + enrollmentDate + '\'' +
                ", gradDate='" + graduationDate + '\'' +
                ", isGradueted='" + hasGraduated + '\'' +
                ", finGrades='" + finalGrades + '\'' +
                '}';
    }


}
